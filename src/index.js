import React from "react";
import ReactDOM from "react-dom";
import './index.css';
import logo from './delete.svg';

const reducer = (state, payload) => {
  switch (payload.type) {
    case "HANDLE_CHANGE":
      return {
        ...state,
        [payload.name]: payload.value
      };
    case "ADD_TASK":
      return {
        ...state,
        tasks: [...state.tasks, payload],
        text: "",
       
      };
    case "REMOVE_TASK":
      return {
        ...state,
        tasks: state.tasks.filter(task => task !== payload.value)
      };
    default:
      return state;
  }
};

let tick = () => {
  var d = new Date();
  var hours = d.getHours();
  var minutes = d.getMinutes();
  var seconds = d.getSeconds();
    if (hours <= 9) hours = "0" + hours;
    if (minutes <= 9) minutes = "0" + minutes;
    if (seconds <= 9) seconds = "0" + seconds;
  var time = [hours+':'+minutes+':'+seconds];
  return (time);
};

setInterval(tick, 1000);


const FlavorForm = () => {
  const [state, dispatch] = React.useReducer(reducer, {
    text: "",
    option: "testing",
    tasks: []
  });
  var when = tick();
  
  const handleChange = React.useCallback(
    payload => dispatch({ ...payload, type: "HANDLE_CHANGE" }),
    []
  );

  const handleSubmit = React.useCallback(
    event => {
      const { text, option, time} = state;
      event.preventDefault();
      return dispatch({ type: "ADD_TASK", value: text, taskType: option, timer: when
    });
    },
    [state]
  );

  const removeTask = React.useCallback(
    value => dispatch({ type: "REMOVE_TASK", value }),
    []
  );


  return (
    <>
    <div className="container">
     <div className="block">
      <form onSubmit={handleSubmit}>
        <input className="field"
          value={state.text}
          onChange={({ target }) =>
            handleChange({
              name: "text",
              value: target.value
            })
          }
          type="text"
          placeholder="What you are doing?"
        />
        <div className="circle_menu"/><select className="list"
          value={state.option}
          onChange={({ target }) =>
            handleChange({
              name: "option",
              value: target.value
            })
          }
        >
          <option value="testing">testing</option>
          <option value="working">working</option>
          <option value="resting">resting</option>
        </select>
        <input type="submit" value="Отправить" />
      </form>
      <hr/>
      <TaskList tasks={state.tasks} removeTask={removeTask} />
      </div>
      </div>
    </>
  );
};


const TaskList = ({ tasks, removeTask }) => {
  return (
    <>
      {tasks.map((task, index) => (
        <div className="block_list" key={index}>
          <div className="margin_block">
          <h2>{task.value}</h2>
          <h4>{task.timer}</h4>
          <h3><div className="circle"/>{task.taskType}</h3>
          <button className="remove" onClick={() => removeTask(task)}><img src={logo}/></button>
          </div>
        </div>
      ))}
      
    </>
   );
};

ReactDOM.render(<FlavorForm />, document.getElementById("root"));
